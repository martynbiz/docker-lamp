# Docker LAMP

This is a basic Docker LAMP environment using MariaDB.

## Usage 

```
$ docker-compose up 
```

Access from localhost:8082/